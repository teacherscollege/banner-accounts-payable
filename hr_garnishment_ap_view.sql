-- GAI-1  WFP-SIG  09/28/2018
-- View to select summarized amounts from Garnishment deductions
-- for interface to Accounts Payable.
-- The internal group code GAIDEDUCTIONCODE is checked for
-- deduction codes that are included in the view.
-- The reference number and Vendor ID are retrieved from SDE fields.

create or replace view tcapp.hr_garnishment_ap_view
as
select  em.spriden_id                      employee_id,
        em.spriden_pidm                    employee_pidm,
        nhrdist_year                       pay_year,
        nhrdist_pict_code                  pay_id,
        nhrdist_payno                      pay_number,
        nhrdist_bdca_code                  deduction_code,
		pdrdedn_effective_date             deduction_eff_date,
        ptrcaln_check_date                 check_date,
		substr(gp_goksdif.f_get_sd_text(
		     'PDRDEDN',
			 'AP_REFERENCE_NUMBER',
		     TO_CHAR(PDRDEDN_PIDM)||CHR(1)||PDRDEDN_BDCA_CODE||CHR(1)||TO_CHAR(PDRDEDN_EFFECTIVE_DATE,'RRRRMMDDHH24MISS','NLS_CALENDAR=GREGORIAN'),
			 '1'),1,50)                    ap_ref_number,
		substr(gp_goksdif.f_get_sd_text(
		     'PDRDEDN',
			 'AP_VENDOR_ID',
		     TO_CHAR(PDRDEDN_PIDM)||CHR(1)||PDRDEDN_BDCA_CODE||CHR(1)||TO_CHAR(PDRDEDN_EFFECTIVE_DATE,'RRRRMMDDHH24MISS','NLS_CALENDAR=GREGORIAN'),
			 '1'),1,9)                     ap_vend_id,
		vn.spriden_pidm                    ap_vend_pidm,
        sum(decode(nhrdist_dr_cr_ind, 'C', 
                     nhrdist_amt, 
                     -nhrdist_amt))        deduction_amount
from    spriden em,
        spriden vn,
        pdrdedn a,
        ptrcaln,
        nhrdist
where   em.spriden_pidm        = pdrdedn_pidm
and     em.spriden_change_ind is null
and     vn.spriden_id(+)       =
            substr(gp_goksdif.f_get_sd_text(
		     'PDRDEDN',
			 'AP_VENDOR_ID',
		     TO_CHAR(PDRDEDN_PIDM)||CHR(1)||PDRDEDN_BDCA_CODE||CHR(1)||TO_CHAR(PDRDEDN_EFFECTIVE_DATE,'RRRRMMDDHH24MISS','NLS_CALENDAR=GREGORIAN'),
			 '1'),1,9)
and     vn.spriden_change_ind  is null
and     pdrdedn_pidm           = em.spriden_pidm
and     pdrdedn_status         = 'A'
and     nhrdist_pidm           = pdrdedn_pidm
and     nhrdist_bdca_code      = pdrdedn_bdca_code
and     trunc(pdrdedn_effective_date) <= trunc(nhrdist_trans_date)
and     pdrdedn_effective_date =
       (select max(pdrdedn_effective_date)
        from   pdrdedn
        where  pdrdedn_pidm       = a.pdrdedn_pidm
        and    pdrdedn_bdca_code  = a.pdrdedn_bdca_code
        and     pdrdedn_status    = 'A'
        and     trunc(pdrdedn_effective_date) <= trunc(nhrdist_trans_date))
and     ptrcaln_year      = nhrdist_year
and     ptrcaln_pict_code = nhrdist_pict_code
and     ptrcaln_payno     = nhrdist_payno
and     exists
       (select 'x'
	    from   gtvsdax
		where  gtvsdax_internal_code_group = 'GAIDEDUCTIONCODE'
		and    gtvsdax_internal_code       = nhrdist_bdca_code)
group by
        em.spriden_id,
        em.spriden_pidm,
        nhrdist_year,
        nhrdist_pict_code,
        nhrdist_payno,
        nhrdist_bdca_code,
		pdrdedn_effective_date,
        ptrcaln_check_date,
		substr(gp_goksdif.f_get_sd_text(
		     'PDRDEDN',
			 'AP_REFERENCE_NUMBER',
		     TO_CHAR(PDRDEDN_PIDM)||CHR(1)||PDRDEDN_BDCA_CODE||CHR(1)||TO_CHAR(PDRDEDN_EFFECTIVE_DATE,'RRRRMMDDHH24MISS','NLS_CALENDAR=GREGORIAN'),
			 '1'),1,50),
		substr(gp_goksdif.f_get_sd_text(
		     'PDRDEDN',
			 'AP_VENDOR_ID',
		     TO_CHAR(PDRDEDN_PIDM)||CHR(1)||PDRDEDN_BDCA_CODE||CHR(1)||TO_CHAR(PDRDEDN_EFFECTIVE_DATE,'RRRRMMDDHH24MISS','NLS_CALENDAR=GREGORIAN'),
			 '1'),1,9),
		vn.spriden_pidm;

create or replace public synonym hr_garnishment_ap_view for tcapp.hr_garnishment_ap_view;

COMMENT ON TABLE HR_GARNISHMENT_AP_VIEW IS 'View to select amounts from Garnishment deductions for interface to Accounts Payable.';
COMMENT ON COLUMN HR_GARNISHMENT_AP_VIEW.EMPLOYEE_ID IS 'SPRIDEN ID for the employee.';
COMMENT ON COLUMN HR_GARNISHMENT_AP_VIEW.EMPLOYEE_PIDM IS 'SPRIDEN PIDM for the employee.';
COMMENT ON COLUMN HR_GARNISHMENT_AP_VIEW.PAY_YEAR IS 'Payroll Year from NHRDIST_YEAR.';
COMMENT ON COLUMN HR_GARNISHMENT_AP_VIEW.PAY_ID IS 'Payroll ID from NHRDIST_PICT_CODE.';
COMMENT ON COLUMN HR_GARNISHMENT_AP_VIEW.PAY_NUMBER IS 'Payroll Number from NHRDIST_PAYNO.';
COMMENT ON COLUMN HR_GARNISHMENT_AP_VIEW.DEDUCTION_CODE IS 'Deduction code from NHRDIST_BDCA_CODE';
COMMENT ON COLUMN HR_GARNISHMENT_AP_VIEW.DEDUCTION_EFF_DATE IS 'Effective Date of the Deduction record that is effective as of the Transaction Date of NHRDIST.';
COMMENT ON COLUMN HR_GARNISHMENT_AP_VIEW.CHECK_DATE IS 'Check Date from PTRCALN_CHECK_DATE for the year, pay ID, and pay number.';
COMMENT ON COLUMN HR_GARNISHMENT_AP_VIEW.AP_REF_NUMBER IS 'SDE field on PDRDEDN - AP_REFERENCE_NUMBER.';
COMMENT ON COLUMN HR_GARNISHMENT_AP_VIEW.AP_VEND_ID IS 'SDE field on PDRDEDN - AP_VENDOR_ID.';
COMMENT ON COLUMN HR_GARNISHMENT_AP_VIEW.AP_VEND_PIDM IS 'SPRIDEN PIDM for SPRIDEN_ID equal to SDE field on PDRDEDN - AP_VENDOR_ID.';
COMMENT ON COLUMN HR_GARNISHMENT_AP_VIEW.DEDUCTION_AMOUNT IS 'Amount from NHRDIST_AMT. Sign determined by NHRDIST_DR_CR_IND.';


