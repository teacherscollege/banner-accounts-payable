/* JIRA: GAI-1 */ 
set echo off
set serveroutput on
set feedback off
set time off
set timing off
set pause off
set verify off
set tab off
set wrap off 

-- debug
-- set echo on
-- set feedback on
-- set verify on

/*****************************************************************************
Program:        prxgaip.sql

Author:         wfp-sig

Date Created:   10/02/2018

Description:    Create AP Invoice for Garnishments

Parms:          01) Payroll Calendar Year
                02) Payroll ID
                03) Payroll Number
                04) Audit/Update (A/U)

Output:         prxgaip_oneup.lis - report file.

Revision Hist:

Release  JIRA      By      Date     Description of Change
-------  ----      --      ----     ---------------------
8.0      GAI-1     wfp  10/02/2018  Initial release
*****************************************************************************/
--
-- Set up global variables
--
column  c_release_no noprint new_value cn_release_no
select  '8.0' c_release_no
from    dual;

column  c_run_date  noprint new_value cn_run_date
column  c_run_time  noprint new_value cn_run_time
column  c_user      noprint new_value cn_user
column  c_one_up_no noprint new_value cn_one_up_no

select  user c_user,
        to_char(sysdate, 'fmMonth dd, yyyy') c_run_date, 
        to_char(sysdate, 'hh24:mi')          c_run_time,
        &1                                   c_one_up_no
from    dual;

start gysbsec PRXGAIP

column  gubinst_name noprint new_value cn_gubinst_name
select  gubinst_name
from    gubinst
where   gubinst_key = 'INST';

column  gjbprun_job noprint new_value cn_gjbprun_job
select  distinct
        gjbprun_job
from    gjbprun
where   gjbprun_one_up_no = &cn_one_up_no;

column  c_job_name noprint new_value cn_job_name
select  'PRXGAIP' c_job_name from dual;

column  gjbjobs_title noprint new_value cn_gjbjobs_title
select  gjbjobs_title
from    gjbjobs
where   gjbjobs_name = '&cn_job_name';

--
-- Get Run Parms from gjbprun
--

col parm01 new_value parm01_year

select  upper(gjbprun_value)                parm01
from    gjbprun
where   gjbprun_job = '&cn_job_name'
and     gjbprun_one_up_no = &cn_one_up_no
and     gjbprun_number = '01';

col parm02 new_value parm02_payid

select  upper(gjbprun_value)                parm02
from    gjbprun
where   gjbprun_job = '&cn_job_name'
and     gjbprun_one_up_no = &cn_one_up_no
and     gjbprun_number = '02';

col parm03 new_value parm03_payno

select  upper(gjbprun_value)                parm03
from    gjbprun
where   gjbprun_job = '&cn_job_name'
and     gjbprun_one_up_no = &cn_one_up_no
and     gjbprun_number = '03';

col parm04 new_value parm04_audupd

select  upper(gjbprun_value)                parm04
from    gjbprun
where   gjbprun_job = '&cn_job_name'
and     gjbprun_one_up_no = &cn_one_up_no
and     gjbprun_number = '04';

-- 
-- Processing Section
--

whenever sqlerror rollback;

declare

v_seqno           fobseqn.fobseqn_maxseqno_7%TYPE;
v_invh_code       fabinvh.fabinvh_code%TYPE;
v_vend_inv_code   fabinvh.fabinvh_vend_inv_code%TYPE;
v_invoice_date    fabinvh.fabinvh_invoice_date%TYPE;
v_pmt_due_date    fabinvh.fabinvh_pmt_due_date%TYPE;
v_trans_date      fabinvh.fabinvh_trans_date%TYPE;
v_comm_desc       farinvc.farinvc_comm_desc%TYPE;
v_vend_pidm       fabinvh.fabinvh_vend_pidm%TYPE;
v_atyp_code       spraddr.spraddr_atyp_code%TYPE;
v_addr_seqno      spraddr.spraddr_seqno%TYPE;
v_appr_unit_price farinvc.farinvc_appr_unit_price%TYPE;
v_ap_userid       varchar2(30);
v_invh_rowid      rowid;
v_invc_rowid      rowid;
v_inva_rowid      rowid;
v_hold_suc        gb_common_strings.err_type;
v_hold_err        gb_common_strings.err_type;
v_hold_war        gb_common_strings.err_type;

cursor seqn_cursor is
select fobseqn_maxseqno_7
from   fobseqn
where  fobseqn_seqno_prefix = 'I';

cursor invh_cursor is
select substr(deduction_code  || '-' || to_char(check_date,'MM/DD/YYYY'),1,15),
       sysdate,  /* Invoice date */
       sysdate,  /* Payment due date */
       sysdate,  /* Transaction Date */
       substr(ap_ref_number,1,50),
       ap_vend_pidm,
       ftvvend_vend_check_atyp_code,
       ftvvend_vend_check_addr_seqno,
       deduction_amount,
       gtvsdax_external_code
from   ftvvend,
       spraddr,
       gtvsdax,
       hr_garnishment_ap_view
where  pay_year        = '&parm01_year'
and    pay_id          = '&parm02_payid'
and    pay_number      = &parm03_payno
and    ap_ref_number is not null
and    ftvvend_pidm      = ap_vend_pidm
and    spraddr_pidm      = ftvvend_pidm
and    spraddr_atyp_code = ftvvend_vend_check_atyp_code
and    spraddr_seqno     = ftvvend_vend_check_addr_seqno
and    nvl(spraddr_status_ind,'A') = 'A'
and   (spraddr_to_date is null
   or  trunc(spraddr_to_date) > trunc(sysdate))
and    gtvsdax_internal_code_group = 'GAIDEDUCTIONCODE'
and    gtvsdax_internal_code       = 'APUSER'
and    '&parm04_audupd'            = 'U';

begin

open  seqn_cursor;
fetch seqn_cursor into v_seqno;
close seqn_cursor;

open  invh_cursor;

loop

fetch invh_cursor
into  v_vend_inv_code,
      v_invoice_date,
      v_pmt_due_date,
      v_trans_date,
      v_comm_desc,
      v_vend_pidm,
      v_atyp_code,
      v_addr_seqno,
      v_appr_unit_price,
      v_ap_userid;

exit  when invh_cursor%NOTFOUND;

v_seqno     := v_seqno + 1;
v_invh_code := 'I' || LPAD(TO_CHAR(v_seqno), 7, '0');
       
  gb_common.p_set_context('FB_INVOICE_HEADER', 'INVOICE_FORM', 'MATCH', 'N');
  FB_INVOICE_HEADER.P_CREATE(
    p_CODE                   =>v_invh_code,                 
    p_invoice_prefix_type    =>'',                                    
    p_POHD_CODE              =>'',            
    p_VEND_PIDM              =>v_vend_pidm,            
    p_USER_ID                =>v_ap_userid,    --user ID must have ability to create an invoice.          
    p_VEND_INV_CODE          =>v_vend_inv_code,        
    p_INVOICE_DATE           =>v_invoice_date,         
    p_PMT_DUE_DATE           =>v_pmt_due_date,         
    p_TRANS_DATE             =>v_trans_date,           
    p_CR_MEMO_IND            =>'N',          
    p_ADJT_CODE              =>'',                                    
    p_ADJT_DATE              =>'',                                    
    p_1099_IND               =>'N',             
    p_1099_ID                =>'',              
    p_ITYP_SEQ_CODE          =>'',        
    p_DISC_CODE              =>'',            
    p_TRAT_CODE              =>'',                                    
    p_ADDL_CHRG_AMT          =>'',        
    p_HOLD_IND               =>'N',             
    p_POST_DATE              =>'',                                    
    p_ATYP_CODE              =>v_atyp_code,            
    p_ATYP_SEQ_NUM           =>v_addr_seqno,         
    p_GROUPING_IND           =>'M',         
    p_BANK_CODE              =>'14',      
    p_RUIV_IND               =>'N',             
    p_EDIT_DEFER_IND         =>'N',       
    p_OVERRIDE_TAX_AMT       =>'',     
    p_TGRP_CODE              =>'', 
    p_VEND_CHECK_PIDM        =>'',      
    p_INVOICE_TYPE_IND       =>'D',     
    p_CURR_CODE              =>'',            
    p_DISB_AGENT_IND         =>'N',       
    p_ATYP_CODE_VEND         =>'',       
    p_ATYP_SEQ_NUM_VEND      =>'',    
    p_NSF_ON_OFF_IND         =>'Y',       
    p_SINGLE_ACCTG_IND       =>'Y',     
    p_ONE_TIME_VEND_NAME     =>'',   
    p_ONE_TIME_HOUSE_NUMBER  =>'',  
    p_ONE_TIME_VEND_ADDR1    =>'',  
    p_ONE_TIME_VEND_ADDR2    =>'',  
    p_ONE_TIME_VEND_ADDR3    =>'',  
    p_ONE_TIME_VEND_ADDR4    =>'',  
    p_ONE_TIME_VEND_CITY     =>'',   
    p_ONE_TIME_VEND_STATE    =>'',  
    p_ONE_TIME_VEND_ZIP      =>'',    
    p_ONE_TIME_VEND_NATN     =>'',   
    p_DELIVERY_POINT         =>'',       
    p_CORRECTION_DIGIT       =>'',     
    p_CARRIER_ROUTE          =>'',        
    p_RUIV_INSTALLMENT_IND   =>'N', 
    p_MULTIPLE_INV_IND       =>'N',     
    p_CTRY_CODE_PHONE        =>'',                                    
    p_PHONE_AREA             =>'',                                    
    p_PHONE_NUMBER           =>'',                                    
    p_PHONE_EXT              =>'',                                    
    p_EMAIL_ADDR             =>'',                                    
    p_CTRY_CODE_FAX          =>'',                                    
    p_FAX_AREA               =>'',                                    
    p_FAX_NUMBER             =>'',                                    
    p_FAX_EXT                =>'',                                    
    p_ATTENTION_TO           =>'',                                    
    p_VENDOR_CONTACT         =>'',                                    
    p_ACH_OVERRIDE_IND       =>'',     
    p_ACHT_CODE              =>'',     
    p_ORIGIN_CODE            =>'',                                    
    p_MATCH_REQUIRED         =>'U', 
    p_DATA_ORIGIN            =>'PRXGAIP.SQL', 
    P_VEND_HOLD_OVRD_IND     =>'',
    P_VEND_HOLD_OVRD_USER    =>'',
    p_ROWID_OUT              =>v_invh_rowid);    

  FB_INVOICE_ITEM.P_CREATE(
     p_INVH_CODE              =>v_invh_code,        
     p_POHD_CODE              =>'',        
     p_ITEM                   =>1,             
     p_PO_ITEM                =>'',          
     p_USER_ID                =>v_ap_userid,    --user ID must have ability to create an invoice.          
     p_COMM_CODE              =>'',        
     p_COMM_DESC              =>v_comm_desc,        
     p_UOMS_CODE              =>'',        
     p_PART_PMT_IND           =>'',     
     p_INVD_UNIT_PRICE        =>'',  
     p_INVD_QTY               =>'',         
     p_ACCEPT_QTY             =>'',       
     p_ACCEPT_UNIT_PRICE      =>'',
     p_DISC_AMT               =>0,         
     p_APPR_QTY               =>1,         
     p_APPR_UNIT_PRICE        =>v_appr_unit_price,  
     p_TOL_OVERRIDE_IND       =>'', 
     p_HOLD_IND               =>'N',        
     p_ADDL_CHRG_AMT          =>0,    
     p_TGRP_CODE              =>'',       
     p_DATA_ORIGIN            =>'PRXGAIP.SQL',                   
     p_ROWID_OUT              =>v_invc_rowid);  
--
  FB_INVOICE_ACCTG.P_CREATE(
     p_INVH_CODE              =>v_invh_code,       
     p_POHD_CODE              =>'',       
     p_ITEM                   =>0,            
     p_PO_ITEM                =>'',         
     p_SEQ_NUM                =>1,         
     p_USER_ID                =>v_ap_userid,    --user ID must have ability to create an invoice.         
     p_RUCL_CODE              =>'INNI',
     p_DISC_RUCL_CODE         =>'',  
     p_TAX_RUCL_CODE          =>'',   
     p_ADDL_RUCL_CODE         =>'',  
     p_COAS_CODE              =>'1',       
     p_ACCI_CODE              =>'',       
     p_FUND_CODE              =>'1',       
     p_ORGN_CODE              =>'',       
     p_ACCT_CODE              =>'2261',       
     p_PROG_CODE              =>'',       
     p_ACTV_CODE              =>'',       
     p_LOCN_CODE              =>'',       
     p_BANK_CODE              =>'14',   
     p_DISC_AMT               =>0,        
     p_TAX_AMT                =>0,           
     p_ADDL_CHRG_AMT          =>0,   
     p_APPR_AMT               =>v_appr_unit_price,        
     p_NSF_OVERRIDE_IND       =>'N',
     p_ITYP_SEQ_CODE          =>'',   
     p_PROJ_CODE              =>'',       
     p_SUSP_IND               =>'N',        
     p_APPR_AMT_PCT           =>100,
     p_DISC_AMT_PCT           =>100,    
     p_ADDL_AMT_PCT           =>100,    
     p_TAX_AMT_PCT            =>100,     
     p_DATA_ORIGIN            =>'PRXGAIP.SQL',              
     p_ROWID_OUT              =>v_inva_rowid);                  
--
  fp_invoice.p_complete(  
   p_code             => v_invh_code,
   p_user_id          => v_ap_userid,    --user ID must have ability to create an invoice. 
   p_completion_ind   => 'Y',
   p_success_msg_out  => v_hold_suc,
   p_error_msg_out    => v_hold_err,
   p_warn_msg_out     => v_hold_war );

end loop;

close  invh_cursor;

--
-- Update sequence number

update fobseqn
set    fobseqn_maxseqno_7   = v_seqno
where  fobseqn_seqno_prefix = 'I'
and    '&parm04_audupd'     = 'U';

end;
/

commit;

whenever sqlerror continue none;

--
-- Report Section
--

--
-- Define linesize, pagesize, ttitle, columns, breaks, computes
--

set newpage 0
set linesize 132
set pagesize 59

ttitle  left '&cn_job_name' '.sql &cn_release_no' -
        center '&cn_gubinst_name' -
        right '&cn_run_date' -
        skip -
        left sql.user - 
        center '&cn_gjbjobs_title' -
        right &cn_run_time ' Page' format 999 sql.pno -
        skip -
        skip center 'Pay Year: &parm01_year Pay ID: &parm02_payid ' -
                    'Pay Number: &parm03_payno Audit/Update: &parm04_audupd ' -
        skip2

column "Empl ID"            format a9
column "Ded"                format a3
column "Ded Eff"            format a8
column "Chk Date"           format a8
column "Reference Number"   format a20
column "Vendor ID"          format a9
column "Vendor Name"        format a25
column "AdSt"               format a4
column "Addr End"           format a8
column "Address Message"    format a18
column "Amount"             format 9999.99

clear breaks
break on "Empl ID" nodup on "Ded Code" nodup on report skip 1

compute sum label 'Total' of "Amount" on report

set termout off

spool &3

select employee_id         "Empl ID",
       deduction_code      "Ded",
       to_char(deduction_eff_date,'MM/DD/YY')  "DedEff Date",
       to_char(check_date,'MM/DD/YY')          "Check Date",
       case when ap_ref_number is null
            then '*No AP Ref Number*'
            else substr(ap_ref_number,1,20)
       end                 "Reference Number",
       ap_vend_id          "Vendor ID",
       case when spriden_entity_ind = 'C'
            then substr(spriden_last_name,1,25)
            when spriden_entity_ind = 'P'
            then substr(spriden_last_name || ', ' || spriden_first_name,1,25)
            else '*Missing/Invalid Vendor*'
       end                 "Vendor Name",
       case when spriden_pidm is null
            then '*No Vendor Lookup*'
            when spraddr_pidm is null
            then '*No Addr Type/Seq*'
            when nvl(spraddr_status_ind,'A') <> 'A'
            then '*Inactive: ' || ftvvend_vend_check_atyp_code || '/' || ftvvend_vend_check_addr_seqno || '*'
            when spraddr_to_date is not null and trunc(spraddr_to_date) <= trunc(sysdate)
            then '*Ended: ' || ftvvend_vend_check_atyp_code || '/' || ftvvend_vend_check_addr_seqno || '*'
            else '*Valid: ' || ftvvend_vend_check_atyp_code || '/' || ftvvend_vend_check_addr_seqno || '*'
       end                           "Address Message",
       case when spraddr_pidm is null
            then null
            else nvl(spraddr_status_ind,'A')
       end                           "AdSt",
       to_char(spraddr_to_date,'MM/DD/YY')        "Addr End",
       deduction_amount              "Amount"
from   ftvvend,
       spraddr,
       spriden,
       hr_garnishment_ap_view
where  pay_year        = '&parm01_year'
and    pay_id          = '&parm02_payid'
and    pay_number      = &parm03_payno
and    ftvvend_pidm(+)      = ap_vend_pidm
and    spraddr_pidm(+)      = ftvvend_pidm
and    spraddr_atyp_code(+) = ftvvend_vend_check_atyp_code
and    spraddr_seqno(+)     = ftvvend_vend_check_addr_seqno
and    spriden_pidm(+)      = ap_vend_pidm
and    spriden_change_ind  is null
union all
select employee_id,
       deduction_code,
       to_char(deduction_eff_date,'MM/DD/YY'),
       to_char(check_date,'MM/DD/YY'),
       substr(ap_ref_number,1,20),
       ap_vend_id,
       case when spriden_entity_ind = 'C'
            then substr(spriden_last_name,1,25)
            else substr(spriden_last_name || ', ' || spriden_first_name,1,25)
       end,
       '*Invoice Created*',
       null,
       fabinvh_code,
       farinvc_appr_unit_price
from   fabinvh,
       farinvc,
       ftvvend,
       spraddr,
       spriden,
       hr_garnishment_ap_view
where  pay_year        = '&parm01_year'
and    pay_id          = '&parm02_payid'
and    pay_number      = &parm03_payno
and    ftvvend_pidm      = ap_vend_pidm
and    spraddr_pidm      = ftvvend_pidm
and    spraddr_atyp_code = ftvvend_vend_check_atyp_code
and    spraddr_seqno     = ftvvend_vend_check_addr_seqno
and    spriden_pidm      = ap_vend_pidm
and    spriden_change_ind  is null
and    fabinvh_vend_pidm = ap_vend_pidm
and    trunc(fabinvh_activity_date) = trunc(sysdate)
and    fabinvh_data_origin = 'PRXGAIP.SQL'
and    farinvc_invh_code   = fabinvh_code
order by 1,2,10;

--
-- Print Report Control Information
--

set heading off
set embedded off
set newpage 0

column c_prunsort   noprint
column c_prundesc   format a60
column c_prunvalue  format a30

clear breaks
break on c_prunsort skip 1

select  ' 0'                                 c_prunsort,
        '*** REPORT CONTROL INFORMATION ***' c_prundesc,
        null                                 c_prunvalue
from    dual
union
select  ' 1',
        'SEQUENCE NUMBER: ',
        to_char(&cn_one_up_no)
from dual
union
select  gjbprun_number,
        gjbpdef_desc,
        gjbprun_value
from    gjbpdef,
        gjbprun
where   gjbprun_job = '&cn_job_name'
and     gjbprun_one_up_no = &cn_one_up_no
and     gjbprun_job = gjbpdef_job
and     gjbprun_number = gjbpdef_number
order by 1;

spool off

set termout on

--
-- Cleanup
--

delete  gjbprun
where   gjbprun_job = '&cn_job_name'
and     gjbprun_one_up_no = &cn_one_up_no;

commit;

exit