# Banner Accounts Payable Repository #

This repository contains reports, scripts, integration programs, Pro\*C, Pro\*COBOL, and SQL files from older projects and small projects that do not merit their own repository.

### Contribution guidelines ###

* Anyone who has read access to this repository is welcome to pull it and create a feature branch.
* Feature branches will be merged if approved by an appropriate CIS representative.

### Who do I talk to? ###

* The current manager of this repository is Jeremy Asbury.

### Additional notes ###

* Currently this repository is a work in progress, currently only containing the Wage Garnishments to AP Interface Code. 